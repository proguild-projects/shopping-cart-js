# Shopping Cart Challenge

Build a basic shopping cart web page. It can be used to manage shopping items and view the total cost.


## Goals

The goal is to practice your programming skills with javascript, html/css. Therefore you should try to build without using third-party tools or APIs!

__Soft Rules__

- Only use pure javascript/typescript. 
- No additional JS libraries
- Create your own HTML/CSS! 
- Specialized CSS libraries for things like animation are allowed, but discouraged. 

__Recommended Tools__

- Your favorite code editor!
- Browser of your choice 
- Debugger (VS Code can check all of these boxes with ease!)


## Features

A shopping cart has the following features: 

- Add items 
- Remove items 
- Change quantity
- View cost of each item 
- View total cost for all items 
- View taxes and fees (optional)


## Stretch Goals 

- Add unit tests
